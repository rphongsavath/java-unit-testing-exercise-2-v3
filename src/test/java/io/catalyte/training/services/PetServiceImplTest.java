package io.catalyte.training.services;

import io.catalyte.training.entities.Pet;
import io.catalyte.training.entities.Vaccination;
import io.catalyte.training.exceptions.BadDataResponse;
import io.catalyte.training.exceptions.ResourceNotFound;
import io.catalyte.training.exceptions.ServiceUnavailable;
import io.catalyte.training.repositories.PetRepository;
import io.catalyte.training.repositories.VaccinationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Example;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
class PetServiceImplTest {

    @Mock
    PetRepository petRepository;

    @Mock
    VaccinationService vaccinationService;

    @InjectMocks
    PetServiceImpl petServiceImpl;

    Pet testPet;
    Vaccination testVaccination;
    List<Pet> testList = new ArrayList<>();

    @BeforeEach
    @SuppressWarnings("unchecked")
    public void setUp() {
        //initialize mocks
        MockitoAnnotations.openMocks(this);

        //set up test Pet
        testPet = new Pet("Whopper", "Dog", 1);
        testPet.setId(1L);

        //set up test Vac
        testVaccination = new Vaccination("Rabies", Date.valueOf("2021-01-08"), testPet);
        testVaccination.setId(1L);
        testList.add(testPet);
        when(petRepository.findById(any(Long.class))).thenReturn(Optional.of(testList.get(0)));
        when(petRepository.saveAll(anyCollection())).thenReturn(testList);
        when(petRepository.save(any(Pet.class))).thenReturn(testList.get(0));
        when(petRepository.findAll()).thenReturn(testList);
        when(petRepository.findAll(any(Example.class))).thenReturn(testList);
    }

    @Test
    public void getAllPets() {
        List<Pet> result = petServiceImpl.queryPets(new Pet());
        assertEquals(testList, result);
    }

    @Test
    public void getAllPetsWithSample() {
        List<Pet> result = petServiceImpl.queryPets(testPet);
        assertEquals(testList, result);
    }

    @Test
    public void getAllPetsDBError() {
        // set repo to trigger Data Access Exception
        when(petRepository.findAll()).thenThrow(EmptyResultDataAccessException.class);

        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.queryPets(new Pet()));
    }

    @Test
    public void getPet() {
        Pet result = petServiceImpl.getPet(1L);
        assertEquals(testPet, result);
    }

    @Test
    public void getPetDBError() {
        when(petRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.getPet(1L));
    }

    @Test
    public void getPetNotFound() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());
        Exception exception = assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.getPet(1L));
        String expectedMessage = "Could not locate a Pet with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void addPets() {
        List<Pet> result = petServiceImpl.addPets(testList);
        assertEquals(testList, result);
    }

    @Test
    public void addPetsDBError() {
        when(petRepository.saveAll(anyCollection())).thenThrow(
                new EmptyResultDataAccessException("Database unavailable", 0));

        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.addPets(testList));
    }


        @Test
        public void addPet() {
            Pet result = petServiceImpl.addPet(testPet);
            assertEquals(testPet, result);
        }


        @Test
        public void addPetDBError() {
            when(petRepository.save(any(Pet.class))).thenThrow(
                    new EmptyResultDataAccessException("Database unavailable", 0));
            assertThrows(ServiceUnavailable.class,
                    () -> petServiceImpl.addPet(testPet));
        }
    @Test
    public void updatePetById() {
        Pet result = petServiceImpl.updatePetById(1L, testPet);
        assertEquals(testPet, result);
    }

    @Test
    public void updatePetByIdDBError() {
        when(petRepository.findById(anyLong())).thenThrow(EmptyResultDataAccessException.class);
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.updatePetById(1L, testPet));
    }

    @Test
    public void updatePetByIdNotFound() {
        when(petRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.updatePetById(1L, testPet));
        String expectedMessage = "Could not locate a Pet with the id: 1";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void updatePetByIdBadData() {
        Exception exception = assertThrows(BadDataResponse.class,
                () -> petServiceImpl.updatePetById(2L, testPet));
        String expectedMessage = "Pet ID must match the ID specified in the URL";
        assertEquals(expectedMessage,
                exception.getMessage(),
                () -> "Message did not equal '" + expectedMessage + "', actual message:"
                        + exception.getMessage());
    }

    @Test
    public void deletePet() {
        when(petRepository.existsById(anyLong())).thenReturn(true);
        petRepository.deleteById(1L);
        verify(petRepository).deleteById(any());
    }

    @Test
    public void deletePetError() {
        doThrow(new ServiceUnavailable("Database unavailable")).when(petRepository)
                .existsById(anyLong());
        assertThrows(ServiceUnavailable.class,
                () -> petServiceImpl.deletePet(1L));
    }

    @Test
    public void deleteVaccinationBadID() {
        doThrow(new ResourceNotFound("Database unavailable")).when(petRepository)
                .deleteById(anyLong());
        assertThrows(ResourceNotFound.class,
                () -> petServiceImpl.deletePet(1L));
    }

}//end class