# Java Unit Testing Exercise 2 v3

## Description
This program allows requests for pets and their vacination status for veterianan's offices. This sepcific project test the Methods in the PetServiceImpl. The goal was to have at least 75% coverage.


## Installation
Please open the PetServiceImplTest file on IntelliJ and run the program with coverage. The percentage of coverage for the file will be displayed on the sidebar

## Usage
Use examples liberally, and show the expected output if you can. 

## Authors and acknowledgment
Richy Phongsavath


